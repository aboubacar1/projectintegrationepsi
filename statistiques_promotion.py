'''
    Programme de lecture et d'analyse d'un fichier structuré (CSV)
    statistuqes : H/F de la promotion
'''
import argparse
import datetime as dt
from dateutil import relativedelta as rd
import functions.stats_func as fc

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--path", type=str)
parser.add_argument("-v", "--version", action='store_true')
parser.add_argument("-l", "--list", action='store_true')

args = parser.parse_args()
file = args.path

if args.version:
    print("version 1.2")
else:
    print("version 1.2")
    if file == None :
        file="promotion_B3_B.csv"
    today=dt.datetime.now()
    ans=0
    month=0
    # Boucle de lecture ligne à ligne
    with open(file, 'r') as file :
        nbrf = nbrh = nbro = 0
        # On saute la premier ligne
        next(file)
        for line in file:
            if line[0] == '#':
                continue
            fields = line.strip().split(";")
            gender = fields[2]
            bdate = fields[4]
            bdate = dt.datetime.strptime(bdate,'%d/%m/%Y')
            years, months = fc.deltadateForBirthDay(bdate)
            ans += years
            month+= months
            if args.list:
                print(f"{fields[1]} - {fields[0]} - {bdate} , âge : {years} ans et {months} mois")
            if gender.lower() == 'h':
                nbrh+=1
            elif gender.lower() == 'f':
                nbrf+=1
            else:
                nbro+=1

    nbr_tot = nbrf + nbrh + nbro
    sep="="*80
    print(sep)
    print(f"Nombre total d'élèves : {nbr_tot}")
    print(f"Nombre total de filles : {nbrf} - {fc.convertAndRoundWith2((nbrf/nbr_tot))} %")
    print(f"Nombre total de garçons : {nbrh} - {fc.convertAndRoundWith2((nbrh/nbr_tot))} %")
    print(f"Autres : {nbro} - {fc.convertAndRoundWith2((nbro/nbr_tot))} %")
    print(f" l'age moyen est de: {ans//nbr_tot} ans et  {month//nbr_tot} mois")
    print(sep)
    print("fin du programme...")
        
