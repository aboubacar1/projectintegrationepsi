'''
    Programme de facturation des photocopies
'''
total_facture = 0
while True:
    nb = input("Entrez le nombre de photocopies (Enter=Sortie) ? : ")
    if nb == '':
        break

    try:
        nb = int(nb)
    except ValueError:
        print(f"Entrez une valeur numérique : {nb} ")
        continue

    if nb <= 10:
        facture = nb * 0.1
    elif nb <= 30:
        facture = 10 * 0.1 + (nb - 10) * 0.09
    else:
        facture = 10 * 0.1 + 20 * 0.09 + (nb - 30) * 0.08

    print(f"La facture est de {facture} €")
    total_facture += facture

print(f"La facture globale s'eleve à : {total_facture}")

print("Fin du programme de photocopies")