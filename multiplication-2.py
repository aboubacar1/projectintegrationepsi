'''
    Programme de calcul des multiplications
    Version : 2
'''
while True:
    rep = input("Entrez un nombre (Enter=Sortie) ? : ")
    if rep == '':
        break

    try:
        rep = int(rep)
    except ValueError:
        print(f"Entrez une valeur numérique : {rep} ")
        continue

    for idx in range(1, 11):
        print(f'{rep} X {idx} = {idx*int(rep)}')

print("Fin du programme de multiplication")