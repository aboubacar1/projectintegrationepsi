""" 
    Programme d'imposition
    date : 23/10/2023
    Auteur : TNA 
"""

while True:
    sexe = input(
        "Entrez le code sexe de la personne (Mm:Male/F:Ffemale) (CR=Sortie) "
    ).lower()

    if sexe == '':
        break

    if not (sexe == 'f' or sexe == 'm'):
        print("Le code sexe n'est pas correct, entrez MmFf")
        continue

    age = input("Entrez l'âge : ")
    try:
        age = int(age)
    except ValueError:
        print(f"Entrez une valeur numérique : {age} ")
        continue

    C1 = (sexe == 'm' and age > 20)
    C2 = (sexe == 'f' and (age > 18 and age < 35))

    if C1 or C2:
        print("Zorglubien imposable")
    else:
        print("Zorglubien NON imposable")

print("fin du programme...")