import pytest
import datetime as dt
from dateutil import relativedelta as rd

def convertAndRoundWith2(nbFloat):
    return round(nbFloat*100, 2)

def deltadateForBirthDay(birthDay, today =dt.datetime.now()):
    delta = rd.relativedelta(today,birthDay )
    return delta.years, delta.months

def test_answer():
    assert convertAndRoundWith2((0.258628)) == 25.86
    
def test_birth():
    
    today=dt.date.today()
    dtbornj=today+rd.relativedelta(years=-18)
    dtbornj_minus_1=dtbornj+rd.relativedelta(days=-1)
    dtbornj_plus_1=dtbornj+rd.relativedelta(days=+1)
    ans, month = deltadateForBirthDay(dtbornj)
    assert ans == 18
    ans, month = deltadateForBirthDay(dtbornj_minus_1)
    assert ans == 18
    ans, month = deltadateForBirthDay(dtbornj_plus_1)
    assert ans == 17
