import functions.devb3 as dev3

while True:
    A = dev3.check_value("A")
    if A == '':
        break

    B = dev3.check_value("B")
    C = dev3.check_value("C")
    D = dev3.check_value("D")

    C1 = A > 50

    C2 = B > 50 or C > 50 or D > 50
    C3 = A >= B and A >= C and A >= D
    C4 = A >= 12.5

    if C1:
        print("Elu au prmier tour")
    elif C2 or not C4:
        print("Battu éliminé, sorti ")
    elif C3:
        print("Ballotage favorable")
    else:
        print("Ballotage défavorable")

print("Fin du programme...")