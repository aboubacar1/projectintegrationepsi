def check_value(label):
    # Documentation Interne
    '''
        Fonction permettant la saisie et le contrôle des valeurs de suffrage
        1/ La saisie doit être numérique 
        2/ La saisie doit être > 0 et <= 100
    '''
    while True:
        rep = input(f"Entrez le score du candidat {label} : ? ")
        if rep == '':
            return rep
        try:
            rep = int(rep)
        except ValueError:
            print(f"Entrez une valeur numérique : {rep} ")
            continue
        # Ajout des tests fonctionnels
        if rep < 0 or rep > 100:
            print(f"La valeur doit être comprise entre 0 et 100 : {rep} ")
            continue
        # Ici tous les sont effectués et la valeur est correcte
        return rep


if __name__ == '__main__':
    print("Ce fichier ne contient que des fonctions !!!")
