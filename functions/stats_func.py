import datetime as dt
from dateutil import relativedelta as rd

def convertAndRoundWith2(nbFloat):
    return round(nbFloat*100, 2)


def deltadateForBirthDay(birthDay, today =dt.datetime.now()):
    delta = rd.relativedelta(today, birthDay )
    return delta.years, delta.months

